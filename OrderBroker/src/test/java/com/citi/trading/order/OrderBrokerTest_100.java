package com.citi.trading.order;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Message;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.citi.trading.Trade;
import com.citi.trading.jms.PointToPointClient;
import com.citi.trading.mq.TradeFactory;
import com.citi.trading.mq.TradeMessenger;

/**
Unit tests for the {@link OrderBroker} that relies on actual messaging
over ActiveMQ.

@author Will Provost
*/
public class OrderBrokerTest_100
{
    private static int notRandom = 0; 
    
    private OrderBroker broker;
    private TradeMessenger service;
    private PointToPointClient client;
    private QueueSender sender;
    
    /**
    Start the order broker as a receiver on an ActiveMQ queue.
    */
    @Before
    public  void setUp ()
        throws Exception
    {
        OrderBroker.outcomeGenerator = () -> notRandom++;
        
        broker = new OrderBroker ();
        service = new TradeMessenger ();
        QueueReceiver receiver = service.openToReceive ();
        receiver.setMessageListener (broker);
        
        client = new PointToPointClient (TradeMessenger.REQUEST_QUEUE);
        sender = client.openToSend ();
        
        Thread.sleep (3000);
    }
    
    /**
    Close the broker and underlying connections.
    */
    @After
    public void tearDown ()
        throws Exception
    {
        Thread.sleep (3000);
        
        client.close ();
        service.close ();
    }
    
    @Test
    public void test100 ()
        throws Exception 
    {
        int fails = 0;
        int partials = 0;
        
        List<String> IDs = new ArrayList<> ();
        try ( PointToPointClient replyClient = 
            new PointToPointClient (TradeMessenger.RESPONSE_QUEUE); )
        {
            QueueReceiver receiver = replyClient.openToReceive ();

            for (int i = 1; i <= 100; ++i)
            {
                Trade trade = TradeFactory.createTestPurchase ();
                trade.setSize (i);
                String XML = TradeMessenger.tradeToXML (trade);
                
                Message request = client.getSession ().createTextMessage (XML);
                request.setJMSCorrelationID ("ID" + i);
                request.setJMSReplyTo (replyClient.getQueue());
                sender.send (request);
                
                IDs.add ("ID" + i);
                System.out.println ("SENT REQUEST ID" + i);
            }
            
            Message response = null;
            while ((response = receiver.receive (15000)) != null)
            {
                assertTrue (response instanceof TextMessage);
                
                Trade confirmedTrade = TradeMessenger.tradeFromXML 
                    (((TextMessage) response).getText ());
                
                String ID = response.getJMSCorrelationID ();
                IDs.remove (ID);
                System.out.println ("RECEIVED REQUEST " + ID + "(" + IDs.size() + " to go) " +
                    confirmedTrade.getResult ());

                if (confirmedTrade.getResult () == Trade.Result.REJECTED)
                    ++fails;
                if (confirmedTrade.getResult () == Trade.Result.PARTIALLY_FILLED)
                    ++partials;
            }
        }
        
        assertEquals (2, IDs.size ()); 
        assertEquals (3, fails);      
        assertEquals (20, partials);
    }
    
    @Test @Ignore
    public void test500 ()
        throws Exception 
    {
        int fails = 0;
        int partials = 0;
        
        List<String> IDs = new ArrayList<> ();
        for (int i = 1; i <= 500; ++i)
        {
            Trade trade = TradeFactory.createTestPurchase ();
            trade.setSize (i);
            String XML = TradeMessenger.tradeToXML (trade);
            
            Message request = client.getSession ().createTextMessage (XML);
            request.setJMSCorrelationID ("ID" + i);
            sender.send (request);
            
            IDs.add ("ID" + i);
            System.out.println ("SENT REQUEST ID" + i);
        }
        
        try ( PointToPointClient replyClient = 
            new PointToPointClient (TradeMessenger.RESPONSE_QUEUE); )
        {
            QueueReceiver receiver = replyClient.openToReceive ();
            Message response = null;
            while ((response = receiver.receive (15000)) != null)
            {
                assertTrue (response instanceof TextMessage);
                
                Trade confirmedTrade = TradeMessenger.tradeFromXML 
                    (((TextMessage) response).getText ());
                
                String ID = response.getJMSCorrelationID ();
                IDs.remove (ID);
                System.out.println ("RECEIVED REQUEST " + ID + "(" + IDs.size() + " to go) " +
                    confirmedTrade.getResult ());

                if (confirmedTrade.getResult () == Trade.Result.REJECTED)
                    ++fails;
                if (confirmedTrade.getResult () == Trade.Result.PARTIALLY_FILLED)
                    ++partials;
            }
        }
        
        assertEquals (0, IDs.size ()); 
        assertEquals (100, fails); 
        assertEquals (56.5, partials, 0.6);
    }
    
    /**
    Enable this if testing gets queues gooped up, but generally ignore:
    otherwise we can kill replies in the real, running queues.
    */
    @After @Ignore
    public void cleanReplyQueue ()
        throws Exception
    {
        try ( PointToPointClient replyClient = 
            new PointToPointClient (TradeMessenger.RESPONSE_QUEUE); )
        {
            QueueReceiver receiver = replyClient.openToReceive (); 
            while (receiver.receive (500) != null)
                System.out.println ("Cleaned out message.");
        }
    }
}
