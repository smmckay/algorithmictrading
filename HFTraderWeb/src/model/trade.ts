/**
 * Deserializable encapsulation of a single trade.
 *
 * @author Will Provost
 */
export class Trade {
  ID: number;
  when: string;
  stock: string;
  size: number;
  buy: boolean;
  price: number;

  constructor(ID: number, when: string, stock: string, size: number,
      buy: boolean, price: number) {
    this.ID = ID;
    this.when = when;
    this.stock = stock;
    this.size = size;
    this.buy = buy;
    this.price = price;
  }
}
