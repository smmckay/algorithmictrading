import { Injectable } from '@angular/core';
import { PricePoint } from './price-point';

@Injectable({
  providedIn: 'root'
})
export class PricingService {
  URL: string;



  /**
   * Set up service URL. Bind all methods so our 'this' references
   * make sense. Initialize subscribers array. Start the polling timer.
   */
  constructor() {
    this.URL = "http://localhost:8082/pricing";

    this.handler = this.handler.bind(this);
    this.checkResponseCode = this.checkResponseCode.bind(this);
    this.checkOK = this.checkOK.bind(this);
    this.checkCreated = this.checkCreated.bind(this);
    this.getAllPricePoints = this.getAllPricePoints.bind(this);
    this.getPricePointsByTime = this.getPricePointsByTime.bind(this);
  }

    /**
   * Error handler currently just logs to the console.
   * A popup message box or some other UI should come into play at some point.
   */
  handler(err): any {
    console.log(err);
    return null;
  }

  
  /**
   * Helper to check that the HTTP response code was the expected alue.
   * Either throws an error or returns the response, making the function
   * suitable for use in a fetch/then chain.
   */
  checkResponseCode(response: Response, expected: number): Response {
    if (response.status !== expected) {
      throw Error("Unexpected response code: " + response.status);
    }
    return response;
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 200 OK.
   */
  checkOK(response: Response): Response {
    return this.checkResponseCode(response, 200);
  }

  /**
   * Specialization of checkResponseCode() that expects HTTP 201 Created.
   */
  checkCreated(response: Response): Response {
    return this.checkResponseCode(response, 201);
  }

  /**
   * Parses the given (weakly typed) object and creates the appropriate
   * type of Pricing, holding the appropriate values.
   */
  static makePricing(source: any): PricePoint {
    return new PricePoint(source.id, source.stock, source.timestamp, source.open,
      source.high, source.low, source.close, source.volume);
  }

  /**
   * Calls the HTTP operation and returns a Promise bearing the
   * requested trader object.
   */
  getAllPricePoints(stock: string): Promise<Array<PricePoint>> {
    return fetch(this.URL + "/" + stock + "/all")
      .then(this.checkOK)
      .then(response => response.json())
      .then(pricing => pricing.map(PricingService.makePricing))
      .catch(this.handler);
  }

    /**
   * Calls the HTTP operation and returns a Promise bearing the
   * requested trader object.
   */
  getPricePointsByTime(stock: string, start: string, end: string): Promise<Array<PricePoint>> {
    return fetch(this.URL + "/" + stock + "/" + start + "/" + end)
      .then(this.checkOK)
      .then(response => response.json())
      .then(pricing => pricing.map(PricingService.makePricing))
      .catch(this.handler);
  }

}
