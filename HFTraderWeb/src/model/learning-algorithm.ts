import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a Learning Algorithm strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
export class LearningAlgorithm extends Trader {
  periodsToCollect: number
  learningRate: number

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      periodsToCollect: number, learningRate:number) {
    super("ML", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.periodsToCollect = periodsToCollect
    this.learningRate = learningRate
  }
}
