import { Position } from "./position";
import { Trader } from "./trader";

export class BollingerBands extends Trader {
    period: number;
    purchaseVariation: number;
    exitThreshold: number;
  
    constructor(ID: number, stock: string, size: number, active: boolean,
        stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
        period: number, purchaseVariation: number, exitThreshold: number) {
      super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
      this.purchaseVariation = purchaseVariation;
      this.period = period;
      this.exitThreshold = exitThreshold;
    }
}
