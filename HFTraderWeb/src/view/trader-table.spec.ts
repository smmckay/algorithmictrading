import { Position } from "../model/position";
import { Trade } from "../model/trade";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderTable } from "./trader-table";

describe('TraderTable', () => {

  beforeEach(function() {
    this.mockTraderService = jasmine.createSpyObj("", [
        "subscribe",
        "unsubscribe",
        "notify",
        "setActive"
      ]);

    this.positions = [
        new Position(new Trade(1, "09:00", "ABC", 100, true, 100),
            new Trade(2, "09:02", "ABC", 100, false, 103), 300, 0.03),
        new Position(new Trade(3, "09:06", "ABC", 100, false, 101), null, 0, NaN)
      ];

    this.traders = [
        new TwoMovingAverages(1, "ABC", 100, true, false, this.positions,
            300, 0.03, 30000, 60000, 0.03),
        new TwoMovingAverages(2, "ABC", 100, true, false, [],
            0, NaN, 30000, 60000, 0.03)
      ];

    this.traderTable = new TraderTable
        (this.mockTraderService);
    this.traderTable.latestTraders(this.traders);

    });

  it("reports the correct number of trades", function() {
      expect(this.traderTable.getTotalTrades()).toBe(3);
    });

  it("reports the correct total profit", function() {
      expect(this.traderTable.getTotalProfit()).toBe(300);
    });

  it("calls setActive() on the injected TraderService", function() {
      const ev = { target: { id: "trader1" }};
      this.traderTable.startOrStop(ev, false);
      this.traders[1].active = false;
      this.traderTable.startOrStop(ev, false);
      expect(this.mockTraderService.setActive).toHaveBeenCalledWith(2, false);
      expect(this.mockTraderService.setActive).toHaveBeenCalledWith(2, true);
    });
});
