import { Component, Input } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { PricingService } from "../model/pricing-service";
import { TraderUpdate } from "../model/trader-service";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HistoryComponent } from 'src/app/history/history.component';
import { PricingComponent } from 'src/app/pricing/pricing.component';
import { PricePoint } from 'src/model/price-point';

/**
* Angular component showing all current traders, including their
* type, parameters, state, and profitability.
*
* @author Will Provost
*/
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})

export class TraderTable implements TraderUpdate {
  
  service: TraderService;
  pricingService: PricingService;
  positions;
  viewingHistory: number;
  traders: Array<Trader> = [];
  pricePoints: Array<PricePoint> = [];
  


  /**
  * Helper to format times in mm:ss format.
  */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }
  
  /**
  * Store the injected service references.
  */
  constructor(service: TraderService, private modalService: NgbModal, pricingService: PricingService) {
    this.viewingHistory = -1;
    this.service = service;
    this.pricingService = pricingService;
    service.subscribe(this);
    service.notify();
  }
  
  /**
  * Replace our array of traders with the latest,
  * which will triger a UI update.
  */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
  }

  setPricingHistory(prices: Array<PricePoint>) {
    this.pricePoints = prices;
  }
  
  async getPricingHistory(stock: string){
     await this.pricingService.getAllPricePoints(stock).then((prices) => {this.setPricingHistory(prices)});
  }

  setViewHistory(ID: number){
    this.viewingHistory = ID;
    return this.traders[ID];
  }

  open(ID: number) {
    const modalRef = this.modalService.open(HistoryComponent);
    modalRef.componentInstance.trader = this.setViewHistory(ID);
    console.log(this.setViewHistory(ID));
    
  }

  async viewPricingHistory(stock: string) {
    const modalRef = this.modalService.open(PricingComponent);
    await this.getPricingHistory(stock);
    modalRef.componentInstance.pricePoints = this.pricePoints;
    console.log(this.getPricingHistory(stock));
  }

  /**
  * Helper to derive a label for the trader's state: "Started",
  * "Stopped", or, if deactivated but still closing out a position,
  * "Stopping".
  */
  getState(trader: Trader) {
    if (trader.stopping && trader.active && trader.trades % 2 != 0) {
      return "Stopping"
    } else if (trader.active) {
      return "Started"
    } else {
      return "Stopped"
    }
  }
  
  /**
  * Helper to derive the total number of trades made by this trader.
  */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }
  
  /**
  * Helper to derive the trader's total profit.
  */
  getTotalProfit(): number {
    return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }
  
  /**
  * Finds the trader at the given table index and uses the
  * TraderService component to send an HTTP request to toggle the
  * trader's state.
  */
  startOrStop(ev: any, hardStop: boolean) {
    const index = ev.target.id.replace("trader", "");
    const trader = this.traders[index];
    this.service.setActive(trader.ID, !trader.active);
  }
}

