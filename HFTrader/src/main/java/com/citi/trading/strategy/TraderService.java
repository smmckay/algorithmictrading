package com.citi.trading.strategy;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.trading.WebExceptions;

/**
 * REST web service allowing control of traders. The terminology is a bit
 * shifty here. Throughout the application, we refer to a {@link Strategy} as
 * the persistent state that defines parameters for a particular algorithm,
 * and to a {@link Trader} as the algorithm implementation, which refers to
 * the strategy for its operating parameters. This service works with both 
 * types. At its core it's a CRUD service over strategies; but it is proactive
 * and for example will create the new trader for a new strategy and will 
 * put that trader into action. For the caller, it's all just state 
 * representations, so the caller is really working with strategy objects.
 * 
 * @author Will Provost
 */
@RestController
@RequestMapping("/traders")
@CrossOrigin(origins="http://localhost:4200")
public class TraderService {

	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private ActiveTraders activeTraders;
	
	private static final Logger LOGGER =
	        Logger.getLogger(TraderService.class.getName () + "Violet");
	
	/**
	 * Helper to return an HTTP 404 if the given strategy ID is not found
	 * in the database.
	 */
	private Strategy findOrFail(int ID) {
		Strategy strategy = strategyRepository.findStrategyAndPositions(ID);
		if (strategy == null) {
			throw new WebExceptions.NotFound("No strategy with ID=" + ID + ".");
		}

		return strategy;
	}
	
	@GetMapping("/")
	public String index(){
		return "/index.html";
	}

	/**
	 * Get all strategies -- active and inactive.
	 */
	@GetMapping
	public List<Strategy> getTraders() {
		ArrayList<Strategy> result = new ArrayList<>();
		for (Strategy strategy : strategyRepository.findAll()) {
			result.add(strategy);
		}
		return result;
	}
	
	/**
	 * Gets a strategy by ID. 
	 */
	@GetMapping("{ID}")
	public Strategy getTraderById(@PathVariable("ID") int ID) {
		return findOrFail(ID);
	}
	
	/**
	 * Update a strategy, setting its <strong>active</strong> property to the
	 * given true/false value. This triggers addition to or removal from the
	 * associated {@link ActiveTraders} component, so that an activated trader
	 * will immediately start trading, and a deactivated one will stop.
	 */
	@PutMapping("{ID}/active")
	@Transactional
	public void activateTrader(@PathVariable("ID") int ID, @RequestBody String start) {
		Strategy strategy = findOrFail(ID);
		//LOGGER.info(strategy.getClass().toString() + " | " +  strategy.stringRepresentation());
		//If we're deactivating a trader, check it's status first and update so it closes any open positions before shutting down
		if (start.equals("false")) {
			if (strategy.isActive()) {
				if (strategy.getOpenPosition() != null) {
					LOGGER.info("Trader " + strategy.getId() + " is stopping");
					strategy.setStopping(true);
					//LOGGER.info("Trader " + strategy.getId() + " Has flags Stopping=" + strategy.isStopping() + " Active=" + strategy.isActive());
					strategy = strategyRepository.save(strategy);
					//LOGGER.info(strategy.getClass().toString() + " | " +  strategy.stringRepresentation());
				} else {
					LOGGER.info("Trader " + strategy.getId() + " is stopped");
					strategy.setStopping(false);
					strategy.setActive(false);
					strategy = strategyRepository.save(strategy);
					//LOGGER.info("Trader " + strategy.getId() + " Has flags Stopping=" + strategy.isStopping() + " Active=" + strategy.isActive());
					activeTraders.removeTraderFor(strategy);
				}
			} 
			
		//Else we start the trader if it is currently deactivated
		} else if (start.equals("true")) {
			if (!strategy.isActive()) {
				LOGGER.info("Trader " + strategy.getId() + " has started");
				strategy.setStopping(false);
				strategy.setActive(true);
				strategy = strategyRepository.save(strategy);
				//LOGGER.info("Trader " + strategy.getId() + " Has flags Stopping=" + strategy.isStopping() + " Active=" + strategy.isActive());
				activeTraders.addTraderFor(strategy);
			}
		}
	}
	
	/**
	 * Creates a new strategy, and then creates the associated trader and adds it to
	 * the associated {@link ActiveTraders} component so that it will start trading.
	 */
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Strategy saveStrategy(@RequestBody Strategy newStrategy) {
		if (newStrategy.getId() != 0) {
			throw new WebExceptions.Conflict
				("Do not specify an ID for a new object; you may inadvertently overwrite an existing object.");
		}
		//LOGGER.log(Level.INFO, newStrategy.toString());
		Strategy saved = strategyRepository.save(newStrategy);
		activeTraders.addTraderFor(saved);
		
		return saved;
	}
}
