package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a two-moving-averages trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Will Provost
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBands extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;
	private double exitThreshold;
	private double purchaseVariation;
	private int period;
	
	public BollingerBands() {}
	
	
	public BollingerBands(String stock, int size, int period, double purchaseVariation, double exitThreshold) {
		super(stock, size);
		this.period = period;
		this.purchaseVariation = purchaseVariation;
		this.exitThreshold = exitThreshold;
	}
	
	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	public int getPeriod() {
		return this.period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
	public void setPurchaseVariation(int purchaseVariation) {
		this.purchaseVariation = purchaseVariation;
	}
	
	public double getPurchaseVariation() {
		return this.purchaseVariation;
	}
	
	@Override
	public String toString() {
		return String.format("BollingerBands: [period=%d, purchaseVariation=%1.2f, exit=%1.4f, %s]",
				period, purchaseVariation, exitThreshold, stringRepresentation());
	}
}
