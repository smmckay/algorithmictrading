package com.citi.trading.strategy;

import java.io.Serializable;
import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("C")
public class LearningAlgorithm extends Strategy implements Serializable {

	private static final long serialVersionUID = 1L;
	private int periodsToCollect = 20; //Number of periods to collect history before starting to trade
	private double learningRate = 0.05;
	
	public int getPeriodsToCollect() {
		return periodsToCollect;
	}
	
	public void setPeriodsToCollect(int periods) {
		periodsToCollect = periods;
	}
	
	public double getLearningRate() {
		return learningRate;
	}
	
	public void setLearningRate(double rate) {
		this.learningRate = rate;
	}
	@Override
	public String toString() {
		return String.format("LearningAlgorithm: [startingOffset=%d, %s]",
				periodsToCollect, stringRepresentation());
	}
}
