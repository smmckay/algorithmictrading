package com.citi.trading.strategy;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import org.ejml.simple.*;
import org.ejml.simple.ops.*;
import org.ejml.equation.*;
import java.sql.Timestamp;
import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

/**
 * Implementation of the two-moving-averages trading strategy.
 * We check the long and short rolling average of the closing prices,
 * and track whether one or the other is greater. When they cross, we
 * open a position. Once open, we close on a configurable percentage
 * gain or loss.
 *
 * @author Will Provost
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LearningAlgorithmTrader extends Trader<LearningAlgorithm> {
	private double periodStdDev;
	private double periodAverage;
	private PricePoint currentPrice = null;
	private PricePoint lastPrice = null;
	private double currentZScore;
	
	
	
	private List<PricePoint> inflectionPoints = new ArrayList<PricePoint>();
	private List<PricePoint> purchasePoints = new ArrayList<PricePoint>();
	private boolean increasing;
	private int periodsCollected = 0;
	
	private SimpleMatrix X; //Our X Values in linear regression
	private SimpleMatrix theta; //Our theta Values in logistic regression
	private SimpleMatrix Y; //Our Y values in logistic regression
	private SimpleMatrix predicted; //Our predicted values in logistic regression
	private SimpleMatrix cost;	//Our cost function value
	private SimpleMatrix gradient; //The gradient of our cost function
	
	private static final Logger LOGGER =
	        Logger.getLogger(LearningAlgorithmTrader.class.getName () + "Violet");
	
	
	public LearningAlgorithmTrader(
		PricingSource pricing, OrderPlacer market, StrategyPersistence strategyPersistence) {
		super(pricing, market, strategyPersistence);
	}

	@Override
	public int getNumberOfPeriodsToWatch() {
		return strategy.getPeriodsToCollect();
	}
	
	private double getTimeSinceStart(PricePoint p) {
		double timeSinceStart = p.getTimestamp().getTime() - this.strategy.getStartedTrading().getTime();
		return timeSinceStart;
	}
	
	private double getTimeTrading(PricePoint open, PricePoint close) {
		return (getTimeSinceStart(close) - getTimeSinceStart(open)) / 10000;
	}
	
	private double getProfit(PricePoint open, PricePoint close) {
		return close.getClose() - open.getClose();
	}
	
	private void updateWeights() {
		theta = theta.minus(gradient);
		LOGGER.info("new values of theta" + theta.toString());
	}
	
	private void getGradient() {
		Equation eq = new Equation();
		double m = X.numRows();
		gradient = new SimpleMatrix(theta.numCols(),1);
		eq.alias(gradient, "G", X, "x", predicted, 
				"H", Y, "y", m, "m", strategy.getLearningRate(), "alpha");
		eq.process("G = (1.0 / m) * alpha * (x' * (H - y))");
		LOGGER.info("gradient of cost function " + gradient.toString());
	}
	
	private void getCost() {
		Equation eq = new Equation();
		double m = X.numRows();
		cost = new SimpleMatrix(1, 1);
		eq.alias(cost, "J", predicted, "H", m, "m", Y, "y");
		eq.process("J = (-1.0 / m) * ((y' * log(H)) + ((1 - y)' * log(1 - H)))");
		LOGGER.info("cost of function " + cost.toString());
	}
	
	private void makePredictions() {
		Equation eq = new Equation();
		predicted = new SimpleMatrix(Y.numRows(), 1);
		
		for (int i = 0; i < 10; i++) {
			eq.alias(X, "X", theta, "theta", Y, "Y", predicted, "P");
			eq.process("P = 1 / (1 + exp(-1 * X * theta))");
			LOGGER.info("predicted values " + predicted.toString());
			getCost();
			getGradient();
			updateWeights();
		}
		
	}
	
	private void getTestPoints() {
		int numPoints = 1000;
		int time = 0;
		
		double price = 500.0;
		double lastPrice = 500.0;
		boolean increasing = false;
		boolean firstPrice = true;
		
		double period = 200.0;
		double amp = 0.6;
		int offset = 0;
		for (int i = 0; i < numPoints; i++) {
			PricePoint dataPoint = new PricePoint();
			offset = (offset + 1) % (int) period;
			lastPrice = price;
			price += amp * (Math.sin((offset * (Math.PI * 2)) / period));
			time += 15000;
			dataPoint.setClose(price);
			dataPoint.setTimestamp(new Timestamp(time));
			
			if (price > lastPrice && !increasing && !firstPrice) {
				inflectionPoints.add(dataPoint);
				LOGGER.info("Added increasing point with price: " + price + " at time: " + time);
				increasing = true;
			}
			else if (price < lastPrice && increasing && !firstPrice) {
				inflectionPoints.add(dataPoint);
				LOGGER.info("Added decreasing point with price: " + price + " at time: " + time);
				increasing = false;
			}
			else if (firstPrice) {
				inflectionPoints.add(dataPoint);
				LOGGER.info("Added starting point with price: " + price + " at time: " + time);
				firstPrice = false;
				increasing = price > lastPrice;
			}
			
		}
	}
	
	private void createMatrices() {
		
		int s = inflectionPoints.size();
		int p = purchasePoints.size();
		LOGGER.info("Create matrices with " + (p / 2) + s + " training points ");
		
		
		double[][] dataIn = new double[(p / 2) + ((s * (s - 1)) / 2)][2];
		double[][] actual = new double[(p / 2) + ((s * (s - 1)) / 2)][1];

		
		int count = 0;
		for (int i = 0; i < s - 1; i ++) {
			for (int j = i + 1; j < s; j++) {
				dataIn[count][0] = getTimeTrading(inflectionPoints.get(i), inflectionPoints.get(j));
				dataIn[count][1] = Math.abs(getProfit(inflectionPoints.get(j), inflectionPoints.get(i)));
				actual[count][0] = (j == i + 1) ? 1 : 0;
				count++;
			}
		}
		
		LOGGER.info("Number of combinations" + count);
		
		for (int i = 0; i < purchasePoints.size() - 1; i += 2) {
			dataIn[i][0] = getTimeTrading(purchasePoints.get(i), purchasePoints.get(i + 1));
			dataIn[i][1] = getProfit(purchasePoints.get(i), purchasePoints.get(i + 1));
			actual[i][0] = 0.0;
		}
		
		double[][] params = new double[3][1];
		X = new SimpleMatrix(dataIn);
		Y = new SimpleMatrix(actual);
		theta = new SimpleMatrix(new double[][]{{1.0}, {0.5}, {1.0}});
		
		LOGGER.info("Matrix X: " + X.toString());
		LOGGER.info("Matrix Y: " + Y.toString());
		LOGGER.info("Matrix theta has Cols: " + theta.numCols() + " Rows: " + theta.numRows() + "/n" + theta.toString());
	}
	

	//Check if our price has reached an inflection point (Assuming it is a simple function)
	private void checkForInflection() {
		LOGGER.info("Checking for inflection points on curve");
		if (increasing && this.currentPrice.getClose() < this.lastPrice.getClose()) {
			LOGGER.info("Inflection point found at Price " + lastPrice.getClose() +  " at time " + getTimeSinceStart(lastPrice));
			inflectionPoints.add(lastPrice);
			increasing = false;
		}
		else if (!increasing && this.currentPrice.getClose() > this.lastPrice.getClose()) {
			LOGGER.info("Inflection point found at Price " + lastPrice.getClose() +  " at time " + getTimeSinceStart(lastPrice));
			inflectionPoints.add(lastPrice);
			increasing = true;
		} else {
			increasing = this.currentPrice.getClose() > this.lastPrice.getClose();
		}
	}
	
	private void getLatestData(PriceData data) {
		this.periodStdDev = data.getWindowStdDev(10, PricePoint::getClose);
        this.periodAverage = data.getWindowAverage(10, PricePoint::getClose);
        if (this.currentPrice != null) {
        	 this.lastPrice = this.currentPrice;
        }
        this.currentPrice = data.getData(1).findAny().get();
        LOGGER.info("Next Price Received " + currentPrice.getClose() +  " at time " + getTimeSinceStart(currentPrice));
        if (this.lastPrice != null) {
        	checkForInflection();
        }
        this.currentZScore = ((this.currentPrice.getClose() - this.periodAverage) / periodStdDev);
       
	}
	
	private void getInflectionPoints(PricePoint[] priceArray) {
		PricePoint currPrice = null;
		PricePoint lastPrice = null;
		boolean increaing = false;
		for (PricePoint p : priceArray) {
			inflectionPoints.add(p);
		/**	if (currPrice != null) {
				lastPrice = currPrice;
			} 
			currPrice = p;
			if (lastPrice != null) {
				if (currPrice.getClose() > lastPrice.getClose() && !increasing) {
					LOGGER.info("Inflection point found at Price " + lastPrice.getClose() +  " at time " + getTimeSinceStart(lastPrice));
					increasing = true;
					inflectionPoints.add(lastPrice);
				} 
				else if (currPrice.getClose() < lastPrice.getClose() && increasing) {
					LOGGER.info("Inflection point found at Price " + lastPrice.getClose() +  " at time " + getTimeSinceStart(lastPrice));
					increasing = false;
				}
			} **/
		}
	}
	
	private void getAllData(PriceData data) {
		Stream<PricePoint> allPrices = data.getData(data.getSize());
		PricePoint[] priceArray = allPrices.toArray(PricePoint[]::new);
		getInflectionPoints(priceArray);
		
	}
	
	private void checkBetterClose(PriceData data) {
		int numPositions = this.strategy.getPositions().size();
		if (numPositions > 1 ) {
	        double lastPurchasePrice = this.strategy.getPositions().get(numPositions).getClosingTrade().getPrice();
	        boolean lastIsBuy = this.strategy.getPositions().get(numPositions).getClosingTrade().isBuy();
	        
	        //Better opening price was available, so we want to adjust our trading parameters
	        if (this.currentPrice.getClose() > lastPurchasePrice && !lastIsBuy ||
	        		this.currentPrice.getClose() < lastPurchasePrice && lastIsBuy) {
	        	double squaredError = Math.pow(currentPrice.getClose() - lastPurchasePrice, 2);
	        } 	
		}
	}
	
	private void checkBetterOpen(PriceData data) {
		if (this.strategy.getPositions().size() > 1  && this.strategy.getOpenPosition() != null) {
			
	        double lastPurchasePrice = this.strategy.getOpenPosition().getOpeningTrade().getPrice();
	        boolean lastIsBuy = this.strategy.getOpenPosition().getOpeningTrade().isBuy();
	        
	        //Better opening price was available, so we want to adjust our trading parameters
	        if (this.currentPrice.getClose() > lastPurchasePrice && !lastIsBuy ||
	        		this.currentPrice.getClose() < lastPurchasePrice && lastIsBuy) {
	        	double squaredError = Math.pow(currentPrice.getClose() - lastPurchasePrice, 2);
	        } 		
		}
		
	}
	

	@Override
	protected void handleDataWhenOpen(PriceData data) {
		getLatestData(data);
		periodsCollected += 1;
		if (periodsCollected > strategy.getPeriodsToCollect()) {
			LOGGER.info("Started Making Predictions after " + periodsCollected + " periods");
			createMatrices();
			makePredictions();
		} else if (periodsCollected == 1) {
			inflectionPoints.add(currentPrice);
		}
		
	}

	@Override
	protected void handleDataWhenClosed(PriceData data) {
		getLatestData(data);
		periodsCollected += 1;
		//checkBetterClose(data);
		if (periodsCollected > strategy.getPeriodsToCollect()) {
			LOGGER.info("Started Making Predictions after " + periodsCollected + " periods");
			createMatrices();
			makePredictions();
		} else if (periodsCollected == 1) {
			inflectionPoints.add(currentPrice);
		}
	}
	private boolean calc = true;
	
	@Override
	public void accept(PriceData data) {
		if (calc) {
			getTestPoints();
			createMatrices();
			makePredictions();
			calc = false;
		}
	}

}
