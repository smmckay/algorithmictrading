package com.citi.trading.strategy;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

/**
 * Implementation of the two-moving-averages trading strategy.
 * We check the long and short rolling average of the closing prices,
 * and track whether one or the other is greater. When they cross, we
 * open a position. Once open, we close on a configurable percentage
 * gain or loss.
 *
 * @author Will Provost
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {

	private static final Logger LOGGER =
	        Logger.getLogger(BollingerBandsTrader.class.getName() + "Violet");
	
	private double periodStdDev;
	private double periodAverage;
	private double currentPrice;
	private double currentZScore;
	
	public BollingerBandsTrader(PricingSource pricing, OrderPlacer market, StrategyPersistence strategyPersistence) {
		super(pricing, market, strategyPersistence);
	}
	
	@Override
	public int getNumberOfPeriodsToWatch() {
		return strategy.getPeriod();
	}
	
	/**
	 * Sets the standard deviation in closing prices and average for the strategies period
	 * Also sets the current price and Z score (Std deviations from the mean)
	 */
	private void checkVariation(PriceData data) {
        this.periodStdDev = data.getWindowStdDev(strategy.getPeriod(), PricePoint::getClose);
        this.periodAverage = data.getWindowAverage(strategy.getPeriod(), PricePoint::getClose);
        this.currentPrice = data.getData(1).findAny().get().getClose();
        this.currentZScore = ((this.currentPrice - this.periodAverage) / periodStdDev);
	}

    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
	
	@Override
	protected void handleDataWhenOpen(PriceData data) {
		checkVariation(data);
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = this.currentPrice / openingPrice - 1.0;
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(this.currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}	
	}

	//TODO Purchase Variation an int
	@Override
	protected void handleDataWhenClosed(PriceData data) {
		if (tracking.get()) {
    		checkVariation(data);
    		if ( Math.abs(this.currentZScore) > (strategy.getPurchaseVariation() / 100.0)) {
    			opener.placeOrder((this.currentZScore < 0), this.currentPrice);
    			LOGGER.info("Trader " + strategy.getId() + " opening position as multiple " + Math.abs(this.currentZScore)
    					+ (this.currentZScore < 0 ? " under" : " over") + " standard deviation.");
    		}
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	checkVariation(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
		
	}

}
