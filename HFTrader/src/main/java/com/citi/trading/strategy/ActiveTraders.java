package com.citi.trading.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * This component knows how to map a {@link Strategy} to the correct
 * {@link Trader} for that strategy type. It loads all active strategies
 * from the database at startup, and translates that list into a list of
 * active traders. From that point forward it serves as manager of the 
 * active traders, starting and stopping trading <em>en masse</em>, 
 * and adding and removing traders as they are activated and deactivated.
 * 
 * @author Will Provost
 */
@Component
public class ActiveTraders implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	private StrategyRepository strategyRepository;

	@Autowired
	private ApplicationContext context;

	private Map<Class<? extends Strategy>, Class<? extends Trader<?>>> 
		strategyToTrader = new HashMap<>();
	private List<Trader<?>> traders;
	private boolean trading;

	/**
	 * This is the central map of trader types to strategy types.
	 * Add new bindings here as you develop new strategies/algorithms.
	 */
	public ActiveTraders() {
		strategyToTrader.put(TwoMovingAverages.class, TwoMovingAveragesTrader.class);
		strategyToTrader.put(BollingerBands.class, BollingerBandsTrader.class);
		strategyToTrader.put(LearningAlgorithm.class, LearningAlgorithmTrader.class);
	}
	
	/**
	 * Find the right trader type for the given strategy. Instantiate the trader
	 * as a prototype-scope Spring bean (so that it gets all of its dependencies
	 * injected correctly), give it a reference to the strategy, and return it. 
	 */
	public <S extends Strategy> Trader<?> getTraderForStrategy(S strategy) {
		@SuppressWarnings("unchecked")
		Trader<S> trader = (Trader<S>) 
				context.getBean(strategyToTrader.get(strategy.getClass()));
		trader.setStrategy(strategy);
		return trader;
	}

	/**
	 * When the application context is complete, query for all active 
	 * strategies, create associated traders, and compile our list.
	 */
	public void onApplicationEvent(ContextRefreshedEvent ev) {
		traders = new ArrayList<>();
		for (Strategy strategy : strategyRepository.findActiveStrategies()) {
            traders.add(getTraderForStrategy(strategy));
        }
    }

	/**
	 * Accessor for the current list of active traders.
	 */
	public synchronized List<Trader<? extends Strategy>> get() {
		return Collections.unmodifiableList(traders);
	}
	
	/**
	 * Add an active trader. If we are currently trading, then start that
	 * trader to join all of the others.
	 */
	public synchronized <S extends Strategy> boolean addTraderFor(S strategy) {
		@SuppressWarnings("unchecked")
		Trader<S> trader = (Trader<S>) getTraderForStrategy(strategy);
		
		boolean added = traders.add(trader);
		if (added && trading) {
			trader.startTrading();
		}
		
		return added;
	}
	
	/**
	 * Remove an active trader. If we are currently trading, stop that trader
	 * before removing it from the pool (so that it won't continue to receive
	 * pricing notifications).
	 */
	public synchronized boolean removeTrader(Trader<? extends Strategy> trader) {
		if (trading) {
			trader.stopTrading();
		}
		
		return traders.remove(trader);
	}

	/**
	 * Remove the active trader for the given strategy. If we are currently 
	 * trading, stop that trader before removing it from the pool (so that 
	 * it won't continue to receive pricing notifications).
	 */
	public synchronized boolean removeTraderFor(Strategy strategy) {
		
		Optional<Trader<?>> trader = traders.stream()
				.filter(t -> t.getStrategy().getId() == strategy.getId())
				.findAny();
		if (trader.isPresent()) {
			return removeTrader(trader.get());
		} else {
			return false;
		}
	}
	
	/**
	 * Return an active trader for a given strategy
	 */
	public synchronized Trader<?> returnTraderFor(Strategy strategy) {
		Optional<Trader<?>> trader = traders.stream()
				.filter(t -> t.getStrategy().getId() == strategy.getId())
				.findAny();
		if (trader.isPresent()) {
			return trader.get();
		} else {
			return null;
		}
	}

	/**
	 * Start all of the active traders, so that they will start receiving 
	 * price notifications and potentially executing trades.
	 */
	public synchronized void start() {
		for (Trader<?> trader : traders) {
			trader.startTrading();
		}
		trading = true;
	}

	/**
	 * Stop all active traders.
	 */
	public synchronized void stop() {
		for (Trader<?> trader : traders) {
			trader.stopTrading();
		}
		trading = false;
	}
}
